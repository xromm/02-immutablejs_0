import { List, Map } from 'immutable';

var a = Map({id: 1, name: 'Egor', input:['three','four']})
//var b = Immutable.Map({id: 2, name: 'Nikita', input:['one','two']})


var b = Map({
  status: {
    fetching: false,
		text: 'InitState'
  },
  header: {
		data: List.of({
			name: 'Nikita',
			id: '1'
		},{
			name: 'Egor',
			id: '2'
		},{
			name: 'Pasha',
			id: '3'
		}),
		inputData: {
			date: [''],
			id: [''],
			name: ['Nikita', 'Egor', 'Pasha']
		}
	},
	main: {
		data: {
			article: [''],
			time: [''],
			amount: [''],
			boxes: [''],
			inBox: ['']
		}
	}
});

// push data into header.inputData.name
let w = b.get('header');
console.log(w.inputData.name);
//let ww = w.inputData.name.push('Vasya');

let bb = b.set('header', {...b.get('header'), inputData: {
    ...b.get('header').inputData,
    name: [...b.get('header').inputData.name, 'Vasya'],
    id: ['1']
  }
});
let ww = bb.get('header').inputData;
console.log(ww);

// // add new item into header.data
// let r = b.get('header');
// for (let i = 0; i < r.data.size; i++) {
//   let item = r.data.get(i)
//   console.log(`${item.id}/${item.name}`);
// }
//
// r = b.get('header').data;
// let rr = r.push({name: "Vasya", id: '4'});
// let bb = b.set('header', {...b.get('header'), data: rr})
//
// r = bb.get('header');
// for (let i = 0; i < r.data.size; i++) {
//   let item = r.data.get(i)
//   console.log(`${item.id}/${item.name}`);
// }


// // set data to status item
// let e = b.get('status');
// console.log(b.get('status').text);
//
// let bb = b.set('status', {...b.get('status'), text: 'newInitState'})
//
// console.log(bb.get('status').text);
// console.log(bb.get('status').fetching);
//
// console.log(b);
// console.log(bb);

// let a = new Map();
//
// a.set('1', 'hello');
// a.set(1, 'bye');
//
// console.log(a.get(1));
// console.log(a.get('1'));
//
// let b = a;
//
// a.set(1, 'bye_bye');
//
// console.log(a);
// console.log(b);
