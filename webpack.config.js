var path = require("path");
//  webpack-dev-server --content-base build/ --inline --hot
module.exports = {
	entry: {
		app: ["./src/index.js", "webpack/hot/dev-server"]
	},
	output: {
		path: path.resolve(__dirname, "build"),
		publicPath: "/assets/",
		filename: "bundle.js"
	},
	module: {
		loaders: [{
			test: /\.js$/,
			exclude: /(node_modules|bower_components)/,
			loader: 'babel-loader', // 'babel-loader' is also a legal name to reference
			query: {
				presets: ['es2015', "stage-0"],
				cacheDirectory: true
			}
		}],
	},
};



// const path = require('path');
// const webpack = require('webpack');
//
// module.exports = {
// 	entry: ['./src/index.js','webpack/hot/dev-server'],
// 	output: {
// 		path: path.resolve(__dirname, 'dist'),
// 		publicPath: "/dist/",
// 		filename: 'bundle.js'
// 	},
// 	devServer: {
// 		historyApiFallback: true,
// 		hot: true,
// 		inline: true,
// 		colors: true,
// 		progress: true,
// 		contentBase: __dirname + '/dist',
// 		host: 'localhost',
// 		port: 8080,
// 	},
// 	// plugins: [
// 	// 	new webpack.optimize.UglifyJsPlugin({
// 	// 		compressor: {
// 	// 			warnings: false,
// 	// 		},
// 	// 	}),
// 	// 	new webpack.optimize.OccurrenceOrderPlugin()
// 	// ],
// 	module: {
// 		loaders: [{
// 			test: /\.js$/,
// 			exclude: /(node_modules|bower_components)/,
// 			loader: 'babel-loader', // 'babel-loader' is also a legal name to reference
// 			query: {
// 				presets: ['es2015'],
//         cacheDirectory: true
// 			}
// 		}]
// 	}
// }
